package org.i9.slb.platform.anseko.libs.net.rfb.server;

import org.i9.slb.platform.anseko.libs.net.rfb.Colour;
import org.i9.slb.platform.anseko.libs.net.rfb.PixelFormat;
import org.i9.slb.platform.anseko.libs.net.rfb.Rect;
import org.i9.slb.platform.anseko.libs.net.server.VNCHost;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Iterator;
import java.util.Vector;

public class RFBSocket
        implements RFBClient, Runnable {
    private Socket socket;
    private VNCHost host;
    private RFBAuthenticator authenticator;
    private RFBServer server = null;
    private DataInputStream input;
    private DataOutputStream output;
    private PixelFormat pixelFormat = null;
    private String protocolVersionMsg = "";
    private boolean shared = true;
    private int[] encodings = new int[0];
    private int preferredEncoding = 5;
    private boolean isRunning = false;
    private boolean threadFinished = false;
    private Vector updateQueue = new Vector();
    private boolean updateAvailable = true;
    private String mapid;

    public RFBSocket(Socket paramSocket, RFBServer paramRFBServer, VNCHost paramVNCHost, RFBAuthenticator paramRFBAuthenticator, String mapid)
            throws IOException {
        this.socket = paramSocket;
        this.mapid = mapid;
        this.server = paramRFBServer;
        this.host = paramVNCHost;
        this.authenticator = paramRFBAuthenticator;
        this.input = new DataInputStream(new BufferedInputStream(paramSocket.getInputStream()));
        this.output = new DataOutputStream(new BufferedOutputStream(paramSocket.getOutputStream(), 16384));
        new Thread(this).start();
    }

    public RFBSocket(Socket paramSocket, RFBServer paramRFBServer, VNCHost paramVNCHost, RFBAuthenticator paramRFBAuthenticator, String mapid, boolean paramBoolean)
            throws IOException {
        this.socket = paramSocket;
        this.mapid = mapid;
        this.server = paramRFBServer;
        this.host = paramVNCHost;
        this.authenticator = paramRFBAuthenticator;
        this.input = new DataInputStream(new BufferedInputStream(paramSocket.getInputStream()));
        this.output = new DataOutputStream(new BufferedOutputStream(paramSocket.getOutputStream(), 16384));
        if (paramBoolean)
            run();
        else
            new Thread(this).start();
    }

    public synchronized void streamToString(DataInputStream dis) throws IOException {
        StringBuffer inputLine = new StringBuffer();
        String tmp;

        BufferedReader d = new BufferedReader(new InputStreamReader(dis));
//    System.out.println("stream ToString:"+d.read());
//        while ((tmp = d.readLine()) != null) {
//        inputLine.append(tmp);
//        System.out.println("Get Client:"+tmp);
//    }
    }

    public synchronized PixelFormat getPixelFormat() {
        return this.pixelFormat;
    }

    public synchronized String getProtocolVersionMsg() {
        return this.protocolVersionMsg;
    }

    public synchronized boolean getShared() {
        return this.shared;
    }

    public synchronized int getPreferredEncoding() {
        return this.preferredEncoding;
    }

    public synchronized void setPreferredEncoding(int paramInt) {
        if (this.encodings.length > 0)
            for (int i = 0; ; ++i) {
                if (i >= this.encodings.length)
                    return;
                if (paramInt != this.encodings[i])
                    continue;
                this.preferredEncoding = paramInt;
                return;
            }
        this.preferredEncoding = paramInt;
    }

    public synchronized int[] getEncodings() {
        return this.encodings;
    }

/*  public synchronized void writeFrameBufferUpdate(Rect[] paramArrayOfRect)
    throws IOException
  {
    writeServerMessageType(0);
    this.output.writeByte(0);
    int i = 0;
    for (int j = 0; j < paramArrayOfRect.length; ++j)
      i += paramArrayOfRect[j].count;
    this.output.writeShort(i);
    for (j = 0; j < paramArrayOfRect.length; ++j)
      paramArrayOfRect[j].writeData(this.output);
    this.output.flush();
  }*/

    public synchronized void writeFrameBufferUpdate(Rect arect[])
            throws IOException {
        writeServerMessageType(0);
        output.writeByte(0);
        int i = 0;
        for (int j = 0; j < arect.length; j++)
            i += arect[j].count;

        output.writeShort(i);
        for (int k = 0; k < arect.length; k++)
            arect[k].writeData(output);

        output.flush();
    }

    public synchronized void writeSetColourMapEntries(int paramInt, Colour[] paramArrayOfColour)
            throws IOException {
        writeServerMessageType(1);
        this.output.writeByte(0);
        this.output.writeShort(paramInt);
        this.output.writeShort(paramArrayOfColour.length);
        for (int i = 0; i < paramArrayOfColour.length; ++i) {
            this.output.writeShort(paramArrayOfColour[i].r);
            this.output.writeShort(paramArrayOfColour[i].g);
            this.output.writeShort(paramArrayOfColour[i].b);
        }
        this.output.flush();
    }

    public synchronized void writeBell()
            throws IOException {
        writeServerMessageType(2);
    }

    public synchronized void writeServerCutText(String paramString)
            throws IOException {
        writeServerMessageType(3);
        this.output.writeByte(0);
        this.output.writeShort(0);
        this.output.writeInt(paramString.length());
        this.output.writeBytes(paramString);
        this.output.writeByte(0);
        this.output.flush();
    }

    public synchronized void close() {
        this.host.close(this);
        if (!(this.isRunning))
            return;
        this.isRunning = false;
        while (!(this.threadFinished))
            try {
                Thread.currentThread();
                Thread.sleep(20L);
            } catch (InterruptedException localInterruptedException) {
            }
        try {
            if ((this.socket != null) && (this.socket.getInetAddress() != null))
                System.out.println("MSG9" + this.socket.getInetAddress().getHostAddress());
            this.socket.close();
        } catch (IOException localIOException) {
        } finally {
            this.socket = null;
        }
    }

    public void run() {
        this.isRunning = true;
        try {
            System.out.println("begin to get version informations");
            writeProtocolVersionMsg();
            readProtocolVersionMsg();
            if (!(this.authenticator.authenticate(this.input, this.output, this))) {
                System.out.println("Authentiation failed");
                return;
            }
            readClientInit();
            initServer();
            writeServerInit();
            System.out.println("data stream input:" + this.input);
            while ((this.isRunning) && (this.input.available() >= 0)) {
                if (getUpdateIsAvailable()) {
                    doFrameBufferUpdate();
                }
                if (this.input.available() == 0)
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException localInterruptedException) {
                    }
                switch (this.input.readUnsignedByte()) {
                    case 0:
                        System.out.println("read pixel format");
                        readSetPixelFormat();
                        break;
                    case 1:
                        System.out.println("read readFixColourMapEntries ");
                        readFixColourMapEntries();
                        break;
                    case 2:
                        System.out.println("read readSetEncodings ");
                        readSetEncodings();
                        break;
                    case 3:
                        readFrameBufferUpdateRequest();
                        break;
                    case 4:
                        System.out.println("read readKeyEvent ");
                        readKeyEvent();
                        break;
                    case 5:
                        readPointerEvent();
                        break;
                    case 6:
                        System.out.println("read readClientCutText ");
                        readClientCutText();
                }
            }
        } catch (IOException localIOException) {
            localIOException.printStackTrace();
            System.out.println("Got an IOException, drop the client");
        } catch (Throwable localThrowable) {
            localThrowable.printStackTrace();
        } finally {
            this.threadFinished = true;
            if (this.server != null)
                this.server.removeClient(this);
            close();
        }
    }

    private void initServer()
            throws IOException {
        if (this.shared)
            this.server = this.host.getSharedServer();
        this.server.addClient(this);
        this.server.setClientProtocolVersionMsg(this, this.protocolVersionMsg);
        this.server.setShared(this, this.shared);
    }

    private synchronized void writeProtocolVersionMsg()
            throws IOException {
        this.output.writeBytes("RFB 003.003\n");/* Ŀǰ 3.3 3.7 3.8 �Ƚ��ȶ�*/
        this.output.flush();
    }

    private synchronized void readProtocolVersionMsg()
            throws IOException {
        byte[] arrayOfByte = new byte[12];
        this.input.readFully(arrayOfByte);
        this.protocolVersionMsg = new String(arrayOfByte);
        System.out.println("protocol Version Message:" + this.protocolVersionMsg);
    }

    private synchronized void readClientInit()
            throws IOException {
        this.streamToString(this.input);
        this.shared = (this.input.readUnsignedByte() == 1);
    }

    private synchronized void writeServerInit()
            throws IOException {
        this.output.writeShort(this.server.getFrameBufferWidth(this));
        this.output.writeShort(this.server.getFrameBufferHeight(this));
        this.server.getPreferredPixelFormat(this).writeData(this.output);
        this.output.writeByte(0);
        this.output.writeByte(0);
        this.output.writeByte(0);
        String str = this.server.getDesktopName(this);
        System.out.println("desktop name:" + str);
        this.output.writeInt(str.length());
        this.output.writeBytes(str);
        this.output.flush();
    }

    private synchronized void writeAuthScheme()
            throws IOException {
        this.output.writeInt(this.authenticator.getAuthScheme(this));
        this.output.flush();
    }

    private synchronized void writeServerMessageType(int paramInt)
            throws IOException {
        this.output.writeByte(paramInt);
    }

    private synchronized void readSetPixelFormat()
            throws IOException {
        this.input.readUnsignedByte();
        this.input.readUnsignedShort();
        this.pixelFormat = new PixelFormat(this.input);
        this.input.readUnsignedByte();
        this.input.readUnsignedShort();
        this.server.setPixelFormat(this, this.pixelFormat);
    }

    private synchronized void readFixColourMapEntries()
            throws IOException {
        this.input.readUnsignedByte();
        int i = this.input.readUnsignedShort();
        int j = this.input.readUnsignedShort();
        Colour[] arrayOfColour = new Colour[j];
        for (int k = 0; k < j; ++k)
            arrayOfColour[k].readData(this.input);
        this.server.fixColourMapEntries(this, i, arrayOfColour);
    }

    private synchronized void readSetEncodings()
            throws IOException {
        this.input.readUnsignedByte();
        int i = this.input.readUnsignedShort();
        this.encodings = new int[i];
        for (int j = 0; j < i; ++j)
            this.encodings[j] = this.input.readInt();
        this.preferredEncoding = Rect.bestEncoding(this.encodings);
        this.server.setEncodings(this, this.encodings);
    }

    private synchronized void readFrameBufferUpdateRequest()
            throws IOException {
        boolean bool = this.input.readUnsignedByte() != 0;
        int i = this.input.readUnsignedShort();
        int j = this.input.readUnsignedShort();
        int k = this.input.readUnsignedShort();
        int l = this.input.readUnsignedShort();
        UpdateRequest localUpdateRequest = new UpdateRequest(bool, i, j, k, l);
        System.out.println("read readFrameBufferUpdateRequest, x : " + i + ", y : " + j + ", w : " + k + ", h : " + l);
        synchronized (this.updateQueue) {
            int i1 = this.updateQueue.indexOf(localUpdateRequest);
            //if (i1 >= 0) {
            //    if (!(localUpdateRequest.incremental))
            //        this.updateQueue.setElementAt(localUpdateRequest, i1);
            //} else
                this.updateQueue.add(localUpdateRequest);
        }
    }

    private synchronized void doFrameBufferUpdate()
            throws IOException {
        Iterator localIterator = this.updateQueue.iterator();
        while (localIterator.hasNext()) {
            UpdateRequest localUpdateRequest = (UpdateRequest) localIterator.next();
            localIterator.remove();
            try {
                System.out.println("RFBSocket is doing an update");
                this.server.frameBufferUpdateRequest(this, localUpdateRequest.incremental, localUpdateRequest.x, localUpdateRequest.y, localUpdateRequest.w, localUpdateRequest.h);
                System.out.println("RFBSocket is done");
            } catch (IOException localIOException) {
                if (localIOException.getMessage().startsWith("rects.length == 0"))
                    this.server.frameBufferUpdateRequest(this, false, 0, 0, 1, 1);
                else
                    throw localIOException;
            } finally {
                //setUpdateIsAvailable(false);
            }
        }
    }

    private synchronized void readKeyEvent()
            throws IOException {
        boolean bool = this.input.readUnsignedByte() == 1;
        this.input.readUnsignedShort();
        int i = this.input.readInt();
        this.server.keyEvent(this, bool, i);
    }

    private synchronized void readPointerEvent()
            throws IOException {
        int i = this.input.readUnsignedByte();
        int j = this.input.readUnsignedShort();
        int k = this.input.readUnsignedShort();
        System.out.println("read readPointerEvent, i : " + i + ", j : " + j + ", k : " + k);
        //this.server.pointerEvent(this, i, j, k);
    }

    private synchronized void readClientCutText()
            throws IOException {
        this.input.readUnsignedByte();
        this.input.readUnsignedShort();
        int i = this.input.readInt();
        byte[] arrayOfByte = new byte[i];
        this.input.readFully(arrayOfByte);
        String str = new String(arrayOfByte);
        this.server.clientCutText(this, str);
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }

    public String getName() {
        return this.host.getDisplayName();
    }

    public void setUpdateIsAvailable(boolean paramBoolean) {
        this.updateAvailable = paramBoolean;
    }

    public boolean getUpdateIsAvailable() {
        return this.updateAvailable;
    }

    private class UpdateRequest {
        boolean incremental;
        int x;
        int y;
        int w;
        int h;

        public UpdateRequest(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
            this.incremental = paramBoolean;
            this.x = paramInt1;
            this.y = paramInt2;
            this.w = paramInt3;
            this.h = paramInt4;
        }

        public boolean equals(Object paramObject) {
            if (paramObject instanceof UpdateRequest) {
                UpdateRequest localUpdateRequest = (UpdateRequest) paramObject;
                return ((this.x == localUpdateRequest.x) && (this.y == localUpdateRequest.y) && (this.w == localUpdateRequest.w) && (this.h == localUpdateRequest.h));
            }
            return false;
        }
    }
}
