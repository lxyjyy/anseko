package org.i9.slb.platform.anseko.libs.net.rfb;

public class KeyCode {

    public int key;
    public boolean isShift;
    public boolean isAlt;
    public boolean isCtrl;

    public KeyCode() {
    }
}
