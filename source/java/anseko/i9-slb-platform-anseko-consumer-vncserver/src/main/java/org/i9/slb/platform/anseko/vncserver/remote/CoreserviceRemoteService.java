package org.i9.slb.platform.anseko.vncserver.remote;

import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.utils.AssertUtil;
import org.i9.slb.platform.anseko.provider.IDubboVNCServerRemoteService;
import org.i9.slb.platform.anseko.provider.dto.GuacamoleConnectDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 调用coreservice远程服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:16
 */
@Service
public class CoreserviceRemoteService {

    @Autowired
    private IDubboVNCServerRemoteService dubboVNCServerRemoteService;

    //private static final Logger LOGGER = LoggerFactory.getLogger(CoreserviceRemoteService.class);

    public GuacamoleConnectDto remoteServiceSimulatorVNCGuacamoleParam(String simulatorId) {
        DubboResult<GuacamoleConnectDto> dubboResult = this.dubboVNCServerRemoteService.simulatorVNCGuacamoleParam(simulatorId);
        //LOGGER.info(JSONObject.toJSONString(dubboResult));
        AssertUtil.isTrue(dubboResult.getResult().intValue() == ErrorCode.SUCCESS.intValue(), dubboResult.getMessage());
        return dubboResult.getRe();
    }
}
