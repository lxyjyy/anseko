package org.i9.slb.platform.anseko.vncserver.remote;

import org.springframework.beans.BeansException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;

/**
 * SpringBeanService 类
 *
 * @author R12
 * @date 2018.08.31
 */
@Component
public class SpringBeanService {

    /**
     * 取得所有的spring-bean
     *
     * @param requiredType
     * @return
     * @throws BeansException
     */
    public static <T> T getBean(Class<T> requiredType) throws BeansException {
        return (T) ContextLoader.getCurrentWebApplicationContext().getBean(requiredType);
    }

    /**
     * 取得环境变量信息
     *
     * @return
     */
    public static Environment getEnvironment() {
        Environment environment = (Environment) ContextLoader.getCurrentWebApplicationContext().getBean(Environment.class);
        return environment;
    }
}
