package org.i9.slb.platform.anseko.downstream;

import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;

import java.util.List;

/**
 * file命令远程服务调用
 *
 * @author R12
 * @date 2018.08.29
 */
public interface IFileCommandExecuteRemoteService {
    /**
     * 执行单条文件命令
     *
     * @param clientId
     * @param fileCommandParamDto
     */
    void fileCommandExecute(String clientId, FileCommandParamDto fileCommandParamDto);

    /**
     * 批量执行文件命令
     *
     * @param clientId
     * @param fileCommandParamDtos
     */
    void fileCommandExecuteBatch(String clientId, List<FileCommandParamDto> fileCommandParamDtos);

}
