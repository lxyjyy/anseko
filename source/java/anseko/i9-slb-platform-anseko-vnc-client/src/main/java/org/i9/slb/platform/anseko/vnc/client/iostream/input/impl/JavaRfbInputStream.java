package org.i9.slb.platform.anseko.vnc.client.iostream.input.impl;

import org.i9.slb.platform.anseko.vnc.client.exception.IoStreamException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;

import java.io.InputStream;

/**
 * java式InputStream
 *
 * @author jiangtao
 * @date 2019年1月24日 09:43:12
 */
public class JavaRfbInputStream extends RfbInputStream {

    private static final int defaultBufSize = 8192;

    private static final int minBulkSize = 1024;

    private InputStream inputStream;

    private int ptrOffset;

    private int bufSize;

    boolean timing;

    long timeWaitedIn100us;

    long timedKbits;

    public JavaRfbInputStream(java.io.InputStream inputStream, int bufSize) {
        this.inputStream = inputStream;
        this.bufSize = bufSize;
        this.buf = new byte[bufSize];
        ptr = end = ptrOffset = 0;
        timeWaitedIn100us = 5;
        timedKbits = 0;
    }

    public JavaRfbInputStream(InputStream inputStream) {
        this(inputStream, defaultBufSize);
    }

    @Override
    public void readBytes(byte[] data, int offset, int length) {
        if (length < minBulkSize) {
            super.readBytes(data, offset, length);
            return;
        }

        int n = end - ptr;
        if (n > length) {
            n = length;
        }

        System.arraycopy(this.buf, ptr, data, offset, n);
        offset += n;
        length -= n;
        ptr += n;

        while (length > 0) {
            n = read(data, offset, length);
            offset += n;
            length -= n;
            ptrOffset += n;
        }
    }

    @Override
    public int pos() {
        return ptrOffset + ptr;
    }

    public void startTiming() {
        timing = true;

        // Carry over up to 1s worth of previous rate for smoothing.

        if (timeWaitedIn100us > 10000) {
            timedKbits = timedKbits * 10000 / timeWaitedIn100us;
            timeWaitedIn100us = 10000;
        }
    }

    public void stopTiming() {
        timing = false;
        if (timeWaitedIn100us < timedKbits / 2) {
            timeWaitedIn100us = timedKbits / 2; // upper limit 20Mbit/s
        }
    }

    public long kbitsPerSecond() {
        return timedKbits * 10000 / timeWaitedIn100us;
    }

    public long timeWaited() {
        return timeWaitedIn100us;
    }

    @Override
    protected int overrun(int itemSize, int nItems) {
        if (itemSize > bufSize) {
            throw new IoStreamException("JavaInStream overrun: MAX itemSize exceeded");
        }
        if (end - ptr != 0) {
            System.arraycopy(this.buf, ptr, this.buf, 0, end - ptr);
        }
        ptrOffset += ptr;
        end -= ptr;
        ptr = 0;

        while (end < itemSize) {
            int n = read(this.buf, end, bufSize - end);
            end += n;
        }

        if (itemSize * nItems > end) {
            nItems = end / itemSize;
        }

        return nItems;
    }

    private int read(byte[] buf, int offset, int len) {
        try {
            long before = 0;
            if (timing) {
                before = System.currentTimeMillis();
            }

            int n = inputStream.read(buf, offset, len);
            if (n < 0) {
                throw new IoStreamException("字节流越界");
            }

            if (timing) {
                long after = System.currentTimeMillis();
                long newTimeWaited = (after - before) * 10;
                int newKbits = n * 8 / 1000;

                // limit rate to between 10kbit/s and 40Mbit/s

                if (newTimeWaited > newKbits * 1000) {
                    newTimeWaited = newKbits * 1000;
                }

                if (newTimeWaited < newKbits / 4) {
                    newTimeWaited = newKbits / 4;
                }

                timeWaitedIn100us += newTimeWaited;
                timedKbits += newKbits;
            }

            return n;

        } catch (java.io.IOException e) {
            throw new IoStreamException("io读取异常");
        }
    }

}
