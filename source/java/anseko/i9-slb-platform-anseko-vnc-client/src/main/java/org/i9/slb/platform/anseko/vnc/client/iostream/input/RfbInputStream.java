package org.i9.slb.platform.anseko.vnc.client.iostream.input;

import org.i9.slb.platform.anseko.vnc.client.exception.IoStreamException;

/**
 * rfb协议基础输入流
 *
 * @auther: jiangtao
 * @date: 2019/1/23 21:16
 */
public abstract class RfbInputStream {

    protected RfbInputStream() {
    }

    // overrun() is implemented by a derived class to cope with buffer overrun.
    // It ensures there are at least itemSize bytes of buffer data.  Returns
    // the number of items in the buffer (up to a maximum of nItems).  itemSize
    // is supposed to be "small" (a few bytes).

    protected abstract int overrun(int itemSize, int nItems);

    // pos() returns the position in the stream.
    public abstract int pos();

    protected byte[] buf;

    protected int ptr;

    protected int end;

    // check() ensures there is buffer data for at least one item of size
    // itemSize bytes.  Returns the number of items in the buffer (up to a
    // maximum of nItems).

    public final int check(int itemSize, int nItems) {
        if (ptr + itemSize * nItems > end) {
            if (ptr + itemSize > end) {
                return overrun(itemSize, nItems);
            }
            nItems = (end - ptr) / itemSize;
        }
        return nItems;
    }

    public final void check(int itemSize) {
        if (ptr + itemSize > end) {
            overrun(itemSize, 1);
        }
    }

    // readU/SN() methods read unsigned and signed N-bit integers.

    public final int readS8() {
        check(1);
        return buf[ptr++];
    }

    public final int readS16() {
        check(2);
        int b0 = buf[ptr++];
        int b1 = buf[ptr++] & 0xff;
        return b0 << 8 | b1;
    }

    public final int readS32() {
        check(4);
        int b0 = buf[ptr++];
        int b1 = buf[ptr++] & 0xff;
        int b2 = buf[ptr++] & 0xff;
        int b3 = buf[ptr++] & 0xff;
        return b0 << 24 | b1 << 16 | b2 << 8 | b3;
    }

    public final int readU8() {
        return readS8() & 0xff;
    }

    public final int readU16() {
        return readS16() & 0xffff;
    }

    public final int readU32() {
        return readS32() & 0xffffffff;
    }

    /**
     * readString() reads a string - a U32 length followed by the data.
     *
     * @return
     */
    public final String readString() {
        int len = readU32();
        if (len > maxStringLength) {
            throw new IoStreamException("InStream MAX string length exceeded");
        }
        char[] str = new char[len];
        int i = 0;
        while (i < len) {
            int j = i + check(1, len - i);
            while (i < j) {
                str[i++] = (char) buf[ptr++];
            }
        }
        return new String(str);
    }

    // maxStringLength protects against allocating a huge buffer.
    // Set it
    // higher if you need longer strings.

    public static int maxStringLength = 65535;

    public final void skip(int bytes) {
        while (bytes > 0) {
            int n = check(1, bytes);
            ptr += n;
            bytes -= n;
        }
    }

    /**
     * readBytes() reads an exact number of bytes into an array at an offset.
     *
     * @param data
     * @param offset
     * @param length
     */
    public void readBytes(byte[] data, int offset, int length) {
        int offsetEnd = offset + length;
        while (offset < offsetEnd) {
            int n = check(1, offsetEnd - offset);
            System.arraycopy(buf, ptr, data, offset, n);
            ptr += n;
            offset += n;
        }
    }

    // readOpaqueN() reads a quantity "without byte-swapping".  Because java has
    // no byte-ordering, we just use big-endian.

    public final int readOpaque8() {
        return readU8();
    }

    public final int readOpaque16() {
        return readU16();
    }

    public final int readOpaque32() {
        return readU32();
    }

    public final int readOpaque24A() {
        check(3);
        int b0 = buf[ptr++];
        int b1 = buf[ptr++];
        int b2 = buf[ptr++];
        return b0 << 24 | b1 << 16 | b2 << 8;
    }

    public final int readOpaque24B() {
        check(3);
        int b0 = buf[ptr++];
        int b1 = buf[ptr++];
        int b2 = buf[ptr++];
        return b0 << 16 | b1 << 8 | b2;
    }

    // bytesAvailable() returns true if at least one byte can be read from the
    // stream without blocking.  i.e. if false is returned then readU8() would
    // block.

    public boolean bytesAvailable() {
        return end != ptr;
    }

    // getbuf(), getptr(), getend() and setptr() are "dirty" methods which allow
    // you to manipulate the buffer directly.  This is useful for a stream which
    // is a wrapper around an underlying stream.

    public byte[] getBuf() {
        return buf;
    }

    public void setBuf(byte[] buf) {
        this.buf = buf;
    }

    public int getPtr() {
        return ptr;
    }

    public void setPtr(int ptr) {
        this.ptr = ptr;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
}
