package org.i9.slb.platform.anseko.vnc.client.rfb.protocol;

import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.RfbOutputStream;

public class PixelFormat {

    public int bpp;

    public int depth;

    public boolean bigEndian;

    public boolean trueColour;

    public int redMax;

    public int greenMax;

    public int blueMax;

    public int redShift;

    public int greenShift;

    public int blueShift;

    public PixelFormat(int bpp, int depth, boolean bigEndian, boolean trueColour) {
        this.bpp = bpp;
        this.depth = depth;
        this.bigEndian = bigEndian;
        this.trueColour = trueColour;
    }

    public PixelFormat(int bpp, int depth, boolean bigEndian, boolean trueColour,
                       int redMax, int greenMax, int blueMax, int redShift, int greenShift, int blueShift) {
        this(bpp, depth, bigEndian, trueColour);
        this.redMax = redMax;
        this.greenMax = greenMax;
        this.blueMax = blueMax;
        this.redShift = redShift;
        this.greenShift = greenShift;
        this.blueShift = blueShift;
    }

    public PixelFormat() {
        this(8, 8, false, true, 7, 7, 3, 0, 3, 6);
    }

    public boolean equal(PixelFormat x) {
        return (bpp == x.bpp &&
                depth == x.depth &&
                (bigEndian == x.bigEndian || bpp == 8) &&
                trueColour == x.trueColour &&
                (!trueColour || (redMax == x.redMax &&
                        greenMax == x.greenMax &&
                        blueMax == x.blueMax &&
                        redShift == x.redShift &&
                        greenShift == x.greenShift &&
                        blueShift == x.blueShift)));
    }

    public void read(RfbInputStream inputStream) {
        this.bpp = inputStream.readU8();
        this.depth = inputStream.readU8();
        this.bigEndian = inputStream.readU8() != 0;
        this.trueColour = inputStream.readU8() != 0;
        this.redMax = inputStream.readU16();
        this.greenMax = inputStream.readU16();
        this.blueMax = inputStream.readU16();
        this.redShift = inputStream.readU8();
        this.greenShift = inputStream.readU8();
        this.blueShift = inputStream.readU8();
        inputStream.skip(3);
    }

    public void write(RfbOutputStream outputStream) {
        outputStream.writeU8(bpp);
        outputStream.writeU8(depth);
        outputStream.writeU8(bigEndian ? 1 : 0);
        outputStream.writeU8(trueColour ? 1 : 0);
        outputStream.writeU16(redMax);
        outputStream.writeU16(greenMax);
        outputStream.writeU16(blueMax);
        outputStream.writeU8(redShift);
        outputStream.writeU8(greenShift);
        outputStream.writeU8(blueShift);
        outputStream.pad(3);
    }

    public String print() {
        StringBuffer s = new StringBuffer();
        s.append("depth " + depth + " (" + bpp + "bpp)");
        if (bpp != 8) {
            if (bigEndian) {
                s.append(" big-endian");
            } else {
                s.append(" little-endian");
            }
        }

        if (!trueColour) {
            s.append(" colour-map");
            return s.toString();
        }

        if (blueShift == 0 && greenShift > blueShift && redShift > greenShift &&
                blueMax == (1 << greenShift) - 1 &&
                greenMax == (1 << (redShift - greenShift)) - 1 &&
                redMax == (1 << (depth - redShift)) - 1) {
            s.append(" rgb" + (depth - redShift) + (redShift - greenShift) + greenShift);
            return s.toString();
        }

        if (redShift == 0 && greenShift > redShift && blueShift > greenShift &&
                redMax == (1 << greenShift) - 1 &&
                greenMax == (1 << (blueShift - greenShift)) - 1 &&
                blueMax == (1 << (depth - blueShift)) - 1) {
            s.append(" bgr" + (depth - blueShift) + (blueShift - greenShift) + greenShift);
            return s.toString();
        }

        s.append(" rgb MAX " + redMax + "," + greenMax + "," + blueMax + " shift " + redShift +
                "," + greenShift + "," + blueShift);
        return s.toString();
    }

}
