package org.i9.slb.platform.anseko.provider.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.provider.entity.InstanceEntity;
import org.i9.slb.platform.anseko.provider.repository.InstanceRepository;
import org.i9.slb.platform.anseko.provider.repository.querybean.InstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实例服务类
 *
 * @author jiangtao
 * @date 2019-02-17
 */
@Service
public class InstanceService {

    @Autowired
    private InstanceRepository instanceRepository;

    /**
     * 获取实例信息
     *
     * @param id
     * @return
     */
    public InstanceEntity getInstanceEntityById(String id) {
        InstanceEntity instanceEntity = this.instanceRepository.getInstanceEntityById(id);
        if (instanceEntity == null) {
            throw new BusinessException("实例不存在");
        }
        return instanceEntity;
    }

    /**
     * 获取实例列表
     *
     * @return
     */
    public List<InstanceEntity> getInstanceEntityList() {
        List<InstanceEntity> list = this.instanceRepository.getInstanceEntityList();
        return list;
    }

    /**
     * 保存实例信息
     *
     * @param instanceEntity
     */
    public void insertInstanceEntity(InstanceEntity instanceEntity) {
        this.instanceRepository.insertInstanceEntity(instanceEntity);
    }

    /**
     * 查询实例名称是否存在
     *
     * @param instanceName
     * @return
     */
    public int getCountInstanceEntityByInstanceNameNumber(String instanceName) {
        int count = this.instanceRepository.getCountInstanceEntityByInstanceNameNumber(instanceName);
        return count;
    }

    /**
     * 查询实例远程地址是否存在
     *
     * @param remoteAddress
     * @return
     */
    public int getCountInstanceEntityByRemoteAddressNumber(String remoteAddress) {
        int count = this.instanceRepository.getCountInstanceEntityByRemoteAddressNumber(remoteAddress);
        return count;
    }

    /**
     * 更新实例信息
     *
     * @param instanceEntity
     */
    public void updateInstanceEntity(InstanceEntity instanceEntity) {
        this.instanceRepository.updateInstanceEntity(instanceEntity);
    }

    /**
     * 判断实例远程地址是否存在
     *
     * @param remoteAddress
     */
    public void checkRemoteAddressIsAlreadyExist(String remoteAddress) {
        int count = this.instanceRepository.getCountInstanceEntityByRemoteAddressNumber(remoteAddress);
        if (count > 0) {
            throw new BusinessException("实例地址已存在");
        }
    }

    /**
     * 判断实例名称是否存在
     *
     * @param instanceName
     */
    public void checkInstanceNameIsAlreadyExist(String instanceName) {
        int count = this.instanceRepository.getCountInstanceEntityByInstanceNameNumber(instanceName);
        if (count > 0) {
            throw new BusinessException("实例名称已存在");
        }
    }

    /**
     * 获取实例列表分页
     *
     * @param instanceQuery
     * @return
     */
    public PageInfo<InstanceEntity> getInstanceEntityListPage(InstanceQuery instanceQuery) {
        PageHelper.startPage(instanceQuery.getStartPage(), instanceQuery.getPageSize());
        List<InstanceEntity> list = this.instanceRepository.getInstanceEntityListPage(instanceQuery);
        PageInfo<InstanceEntity> pageInfo = new PageInfo<InstanceEntity>(list);
        return pageInfo;
    }

    public List<InstanceEntity> getInstanceEntityListOnline() {
        List<InstanceEntity> list = this.instanceRepository.getInstanceEntityListOnline();
        return list;
    }
}