package org.i9.slb.platform.anseko.provider.repository.querybean;

public class InstanceQuery extends BasePageQuery implements java.io.Serializable {

    private static final long serialVersionUID = -7384107631484202511L;

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    private String instanceName;

}
