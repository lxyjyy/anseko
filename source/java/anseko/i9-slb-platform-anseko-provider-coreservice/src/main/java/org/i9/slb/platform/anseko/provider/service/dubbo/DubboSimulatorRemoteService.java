package org.i9.slb.platform.anseko.provider.service.dubbo;

import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.common.utils.DateUtil;
import org.i9.slb.platform.anseko.provider.IDubboSimulatorRemoteService;
import org.i9.slb.platform.anseko.provider.dto.SimulatorAdapterDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDisplayDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorFirewallDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorInfoDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorListDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorSearchDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorStorageDto;
import org.i9.slb.platform.anseko.provider.entity.InstanceEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorAdapterEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorDisplayEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorFirewallEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorStorageEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.SimulatorQuery;
import org.i9.slb.platform.anseko.provider.service.InstanceService;
import org.i9.slb.platform.anseko.provider.service.SimulatorAdapterService;
import org.i9.slb.platform.anseko.provider.service.SimulatorDisplayService;
import org.i9.slb.platform.anseko.provider.service.SimulatorFirewallService;
import org.i9.slb.platform.anseko.provider.service.SimulatorService;
import org.i9.slb.platform.anseko.provider.service.SimulatorStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟器远程数据调用服务
 *
 * @author R12
 * @date 2018年9月6日 17:04:48
 */
@Service("dubboSimulatorRemoteService")
public class DubboSimulatorRemoteService implements IDubboSimulatorRemoteService {

    @Autowired
    private SimulatorService simulatorService;

    @Autowired
    private InstanceService instanceService;

    @Autowired
    private SimulatorAdapterService simulatorAdapterService;

    @Autowired
    private SimulatorFirewallService simulatorFirewallService;

    @Autowired
    private SimulatorStorageService simulatorStorageService;

    @Autowired
    private SimulatorDisplayService simulatorDisplayService;

    /**
     * 获取所有模拟器列表
     *
     * @return
     */
    @Override
    public DubboResult<List<SimulatorDto>> getSimulatorDtos() {
        List<SimulatorDto> list = new ArrayList<SimulatorDto>();
        for (SimulatorEntity simulatorEntity : this.simulatorService.getSimulatorEntityList()) {
            SimulatorDto simulatorDto = new SimulatorDto();
            this.copyProperty(simulatorEntity, simulatorDto);
            list.add(simulatorDto);
        }
        DubboResult<List<SimulatorDto>> dubboResult = new DubboResult<List<SimulatorDto>>();
        dubboResult.setRe(list);
        return dubboResult;
    }

    /**
     * 获取模拟器列表分页
     *
     * @param simulatorSearchDto
     * @return
     */
    @Override
    public DubboResult<SimulatorListDto> getSimulatorDtosPage(SimulatorSearchDto simulatorSearchDto) {
        SimulatorQuery simulatorQuery = new SimulatorQuery();
        simulatorQuery.setStartPage(simulatorSearchDto.getStartPage());
        simulatorQuery.setPageSize(simulatorSearchDto.getPageSize());
        simulatorQuery.setSimulatorName(simulatorSearchDto.getSimulatorName());
        simulatorQuery.setInstanceId(simulatorSearchDto.getInstanceId());
        simulatorQuery.setPowerStatus(simulatorSearchDto.getPowerStatus());

        List<SimulatorDto> list = new ArrayList<SimulatorDto>();
        PageInfo<SimulatorEntity> pageInfo = this.simulatorService.getSimulatorEntityList(simulatorQuery);
        for (SimulatorEntity simulatorEntity : pageInfo.getList()) {
            SimulatorDto simulatorDto = new SimulatorDto();
            this.copyProperty(simulatorEntity, simulatorDto);
            list.add(simulatorDto);
        }

        SimulatorListDto simulatorListDto = new SimulatorListDto();
        simulatorListDto.setList(list);
        simulatorListDto.setTotalRow((int) pageInfo.getTotal());

        DubboResult<SimulatorListDto> dubboResult = new DubboResult<SimulatorListDto>();
        dubboResult.setRe(simulatorListDto);
        return dubboResult;
    }

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    @Override
    public DubboResult<SimulatorDto> getSimulatorDto(String simulatorId) {
        DubboResult<SimulatorDto> dubboResult = new DubboResult<SimulatorDto>();
        try {
            SimulatorEntity simulatorEntity = simulatorService.getSimulatorEntityById(simulatorId);
            SimulatorDto simulatorDto = new SimulatorDto();
            this.copyProperty(simulatorEntity, simulatorDto);
            dubboResult.setRe(simulatorDto);
        } catch (BusinessException e) {
            dubboResult.setBusinessException(e);
        }
        return dubboResult;
    }

    /**
     * 创建模拟器
     */
    @Override
    public synchronized DubboResult<?> createSimulatorInfo(SimulatorDto simulatorDto) {
        try {
            createSimulatorInfo00(simulatorDto);
            return DubboResult.ok();
        } catch (BusinessException e) {
            return DubboResult.error(e);
        }
    }

    private void createSimulatorInfo00(SimulatorDto simulatorDto) {
        InstanceEntity instanceEntity = this.instanceService.getInstanceEntityById(simulatorDto.getInstanceId());

        this.simulatorService.checkSimulatorNameAlreadyExist(simulatorDto.getSimulatorName());

        SimulatorEntity simulatorEntity = new SimulatorEntity();
        simulatorEntity.setId(simulatorDto.getId());
        simulatorEntity.setSimulatorName(simulatorDto.getSimulatorName());
        simulatorEntity.setAndroidVersion(simulatorDto.getAndroidVersion());
        simulatorEntity.setCpunum(simulatorDto.getCpunum());
        simulatorEntity.setRamnum(simulatorDto.getRamnum());
        simulatorEntity.setCreateDate(DateUtil.formatNow());
        simulatorEntity.setInstanceId(instanceEntity.getId());
        simulatorEntity.setPowerStatus(simulatorDto.getPowerStatus());
        simulatorEntity.setCreateDate(DateUtil.formatNow());
        simulatorEntity.setUpdateDate(DateUtil.formatNow());

        this.simulatorService.insertSimulatorEntity(simulatorEntity);
    }

    /**
     * 删除模拟器
     *
     * @param simulatorId
     */
    @Override
    public DubboResult<?> removeSimulatorInfo(String simulatorId) {
        this.simulatorService.deleteSimulatorEntity(simulatorId);
        return DubboResult.ok();
    }

    /**
     * 更新模拟器状态
     *
     * @param simulatorId
     * @param powerState
     */
    @Override
    public DubboResult<?> refreshSimulatorPowerState(String simulatorId, int powerState) {
        this.simulatorService.updateSimulatorEntityPowerState(simulatorId, powerState);
        return DubboResult.ok();
    }

    /**
     * 获取模拟器最大端口号
     *
     * @param instanceId
     * @return
     */
    @Override
    public int getNextSimulatorVNCPortNumber(String instanceId) {
        Integer vncPort = this.simulatorService.getNextSimulatorVNCPortNumber(instanceId);
        return vncPort;
    }

    @Transactional
    @Override
    public DubboResult<?> createSimulatorInfo0(SimulatorInfoDto simulatorInfoDto) {
        try {
            SimulatorDto simulatorDto = simulatorInfoDto.getSimulatorDto();
            // 创建模拟器基础信息
            this.createSimulatorInfo00(simulatorDto);

            // 创建模拟器显示信息
            this.simulatorDisplayService.createSimulatorDisplay(simulatorDto.getId(), simulatorInfoDto.getSimulatorDisplayDto());

            // 创建模拟器端口转发信息
            this.simulatorFirewallService.createSimulatorFirewall(simulatorDto.getId(), simulatorInfoDto.getSimulatorFirewallDtos());

            // 创建模拟器存储信息
            this.simulatorStorageService.createSimulatorStorage(simulatorDto.getId(), simulatorInfoDto.getSimulatorStorageDtos());

            // 保存模拟器网络适配器
            this.simulatorAdapterService.createSimulatorAdapter(simulatorDto.getId(), simulatorInfoDto.getSimulatorAdapterDtos());

            return DubboResult.ok();
        } catch (BusinessException e) {
            return DubboResult.error(e);
        } catch (Exception e) {
            return DubboResult.error(ErrorCode.UNKOWN_ERROR, "未知错误");
        }
    }

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    @Override
    public DubboResult<SimulatorInfoDto> getSimulatorInfoDto(String simulatorId) {
        try {
            SimulatorInfoDto simulatorInfoDto = new SimulatorInfoDto();

            SimulatorDto simulatorDto = new SimulatorDto();
            SimulatorEntity simulatorEntity = this.simulatorService.getSimulatorEntityById(simulatorId);
            this.copyProperty(simulatorEntity, simulatorDto);
            simulatorInfoDto.setSimulatorDto(simulatorDto);

            SimulatorDisplayDto simulatorDisplayDto = new SimulatorDisplayDto();
            SimulatorDisplayEntity simulatorDisplayEntity = this.simulatorDisplayService.getSimulatorDisplayEntity(simulatorId);
            this.simulatorDisplayService.copyProperty(simulatorDisplayEntity, simulatorDisplayDto);
            simulatorInfoDto.setSimulatorDisplayDto(simulatorDisplayDto);

            List<SimulatorFirewallEntity> simulatorFirewallEntityList = this.simulatorFirewallService.getSimulatorFirewallEntityList(simulatorId);
            for (SimulatorFirewallEntity simulatorFirewallEntity : simulatorFirewallEntityList) {
                SimulatorFirewallDto simulatorFirewallDto = new SimulatorFirewallDto();
                this.simulatorFirewallService.copyProperty(simulatorFirewallEntity, simulatorFirewallDto);
                simulatorInfoDto.getSimulatorFirewallDtos().add(simulatorFirewallDto);
            }

            List<SimulatorStorageEntity> simulatorStorageEntityList = this.simulatorStorageService.getSimulatorStorageEntityList(simulatorId);
            for (SimulatorStorageEntity simulatorStorageEntity : simulatorStorageEntityList) {
                SimulatorStorageDto simulatorStorageDto = new SimulatorStorageDto();
                this.simulatorStorageService.copyProperty(simulatorStorageEntity, simulatorStorageDto);
                simulatorInfoDto.getSimulatorStorageDtos().add(simulatorStorageDto);
            }

            List<SimulatorAdapterEntity> simulatorAdapterEntityList = this.simulatorAdapterService.getSimulatorAdapterEntityList(simulatorId);
            for (SimulatorAdapterEntity simulatorAdapterEntity : simulatorAdapterEntityList) {
                SimulatorAdapterDto simulatorAdapterDto = new SimulatorAdapterDto();
                this.simulatorAdapterService.copyProperty(simulatorAdapterEntity, simulatorAdapterDto);
                simulatorInfoDto.getSimulatorAdapterDtos().add(simulatorAdapterDto);
            }
            return DubboResult.ok(simulatorInfoDto);
        } catch (BusinessException e) {
            return DubboResult.error(e);
        } catch (Exception e) {
            return DubboResult.error(ErrorCode.UNKOWN_ERROR, "未知错误");
        }
    }

    private void createSimulatorDisplay(SimulatorDisplayEntity simulatorDisplayEntity) {
    }

    public void copyProperty(SimulatorEntity simulatorEntity, SimulatorDto simulatorDto) {
        simulatorDto.setId(simulatorEntity.getId());
        simulatorDto.setSimulatorName(simulatorEntity.getSimulatorName());
        simulatorDto.setAndroidVersion(simulatorEntity.getAndroidVersion());
        simulatorDto.setCpunum(simulatorEntity.getCpunum());
        simulatorDto.setRamnum(simulatorEntity.getRamnum());
        simulatorDto.setInstanceId(simulatorEntity.getInstanceId());
        simulatorDto.setPowerStatus(simulatorEntity.getPowerStatus());
        simulatorDto.setCreateDate(simulatorEntity.getCreateDate());
        simulatorDto.setUpdateDate(simulatorEntity.getUpdateDate());
    }
}
