package org.i9.slb.platform.anseko.provider.service.dubbo;

import org.i9.slb.platform.anseko.provider.IDubboSimulatorFirewallRemoteService;
import org.i9.slb.platform.anseko.provider.dto.SimulatorFirewallDto;
import org.i9.slb.platform.anseko.provider.service.SimulatorFirewallService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 模拟器端口映射远程服务
 *
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/3/27 16:19
 */
public class DubboSimulatorFirewallRemoteService implements IDubboSimulatorFirewallRemoteService {

    @Autowired
    private SimulatorFirewallService simulatorFirewallService;

    /**
     * 创建模拟器端口映射
     *
     * @param simulatorFirewallDto
     */
    @Override
    public void createSimulatorFirewall(SimulatorFirewallDto simulatorFirewallDto) {
        this.simulatorFirewallService.createSimulatorFirewall(simulatorFirewallDto);
    }
}
