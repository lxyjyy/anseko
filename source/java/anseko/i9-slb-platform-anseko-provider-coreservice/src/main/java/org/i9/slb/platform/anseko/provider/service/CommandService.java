package org.i9.slb.platform.anseko.provider.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.provider.entity.CommandExecuteEntity;
import org.i9.slb.platform.anseko.provider.entity.CommandGroupEntity;
import org.i9.slb.platform.anseko.provider.repository.CommandExecuteRepository;
import org.i9.slb.platform.anseko.provider.repository.CommandGroupRepository;
import org.i9.slb.platform.anseko.provider.repository.querybean.CommandExecuteQuery;
import org.i9.slb.platform.anseko.provider.repository.querybean.CommandGroupQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandService {

    @Autowired
    private CommandGroupRepository commandGroupRepository;

    @Autowired
    private CommandExecuteRepository commandExecuteRepository;

    /**
     * 保存命令调度信息
     *
     * @param commandGroupEntity
     */
    public void insertCommandGroupEntity(CommandGroupEntity commandGroupEntity) {
        this.commandGroupRepository.insertCommandGroupEntity(commandGroupEntity);
    }

    /**
     * 保存命令执行信息
     *
     * @param commandExecuteEntity
     */
    public void insertCommandExecuteEntity(CommandExecuteEntity commandExecuteEntity) {
        this.commandExecuteRepository.insertCommandExecuteEntity(commandExecuteEntity);
    }

    /**
     * 获取命令执行信息
     *
     * @param commandId
     * @return
     */
    public CommandExecuteEntity getCommandExecuteEntityListByCommandId(String commandId) {
        CommandExecuteEntity commandExecuteEntity = this.commandExecuteRepository.getCommandExecuteEntityListByCommandId(commandId);
        if (commandExecuteEntity == null) {
            throw new BusinessException("命令执行不存在");
        }
        return commandExecuteEntity;
    }

    /**
     * 更新命令执行状态
     *
     * @param commandId
     * @param executeResult
     */
    public void updateCommandExecuteEntityCommandResult(String commandId, String executeResult) {
        this.commandExecuteRepository.updateCommandExecuteEntityCommandResult(commandId, executeResult);
    }

    /**
     * 获取命令调度执行列表
     *
     * @param commandGroupId
     * @return
     */
    public List<CommandExecuteEntity> getCommandExecuteEntityListByGroupId(String commandGroupId) {
        List<CommandExecuteEntity> list = this.commandExecuteRepository.getCommandExecuteEntityListByGroupId(commandGroupId);
        return list;
    }

    /**
     * 更新命令调度状态
     *
     * @param commandGroupId
     * @param success
     */
    public void updateCommandGroupEndTime(String commandGroupId, int success) {
        this.commandGroupRepository.updateCommandGroupEndTime(commandGroupId, success);
    }

    /**
     * 获取模拟器命令调度列表
     *
     * @param simulatorId
     * @return
     */
    public List<CommandGroupEntity> getCommandGroupEntityList(String simulatorId) {
        List<CommandGroupEntity> list = this.commandGroupRepository.getCommandGroupEntityList(simulatorId);
        return list;
    }

    /**
     * 获取模拟器命令组列表分页
     *
     * @param commandGroupQuery
     * @return
     */
    public PageInfo<CommandGroupEntity> getCommandGroupEntityListPage(CommandGroupQuery commandGroupQuery) {
        PageHelper.startPage(commandGroupQuery.getStartPage(), commandGroupQuery.getPageSize());
        List<CommandGroupEntity> list = this.commandGroupRepository.getCommandGroupEntityListPage(commandGroupQuery);
        PageInfo<CommandGroupEntity> pageInfo = new PageInfo<CommandGroupEntity>(list);
        return pageInfo;
    }

    /**
     * 获取模拟器执行命令列表分页
     *
     * @param commandExecuteQuery
     * @return
     */
    public PageInfo<CommandExecuteEntity> getCommandExecuteEntityListPage(CommandExecuteQuery commandExecuteQuery) {
        PageHelper.startPage(commandExecuteQuery.getStartPage(), commandExecuteQuery.getPageSize());
        List<CommandExecuteEntity> list = this.commandExecuteRepository.getCommandExecuteEntityListPage(commandExecuteQuery);
        PageInfo<CommandExecuteEntity> pageInfo = new PageInfo<CommandExecuteEntity>(list);
        return pageInfo;
    }

    /**
     * 获取命令组详情
     *
     * @param commandGroupId
     * @return
     */
    public CommandGroupEntity getCommandGroupEntityByGroupId(String commandGroupId) {
        CommandGroupEntity commandGroupEntity = this.commandGroupRepository.getCommandGroupByGroupId(commandGroupId);
        if (commandGroupEntity == null) {
            throw new BusinessException("命令组不存在");
        }
        return commandGroupEntity;
    }
}
