package org.i9.slb.platform.anseko.provider;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.dto.CommandCallbackDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DubboCommandRemoteServiceTest extends JunitBaseTest {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    @Test
    public void testDubboCommandRemoteService() {
        CommandGroupDto commandGroupDto = new CommandGroupDto();
        commandGroupDto.setCommandGroupId(UUIDUtil.generateUUID());
        commandGroupDto.setSimulatorId(UUIDUtil.generateUUID());

        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        for (int i = 0; i < 10; i++) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandLine("dd if=/dev/zero of=/tmp/data.img bs=1G count=100");
            commandExecuteDto.setCommandId(UUID.randomUUID().toString());
            commandExecuteDtos.add(commandExecuteDto);
        }

        commandGroupDto.setCommandExecuteDtos(commandExecuteDtos);
        dubboCommandRemoteService.launchCommandGroup(commandGroupDto);

        for (CommandExecuteDto commandExecuteDto : commandGroupDto.getCommandExecuteDtos()) {
            CommandCallbackDto commandCallbackDto = new CommandCallbackDto();
            commandCallbackDto.setCommandId(commandExecuteDto.getCommandId());
            commandCallbackDto.setExecuteResult("success");
            dubboCommandRemoteService.callbackCommandExecute(commandCallbackDto);
        }
    }

    @Test
    public void testGetCommandGroupDtoListSimulatorId() {
        String simulatorId = "22644e27-cd1e-4680-9304-e44590e643f9";
        DubboResult<List<CommandGroupDto>> dubboResult = this.dubboCommandRemoteService.getCommandGroupDtoListSimulatorId(simulatorId);
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        for (CommandGroupDto commandGroupDto : dubboResult.getRe()) {
            System.out.println(JSONObject.toJSONString(commandGroupDto));
        }
    }

    @Test
    public void testGetCommandExecuteDtosCommandGroupId() {
        String commandGroupId = "c2900ca2-4ceb-46cf-af7a-0cc93978c721";
        DubboResult<List<CommandExecuteDto>> dubboResult = this.dubboCommandRemoteService.getCommandExecuteDtosCommandGroupId(commandGroupId);
        if (dubboResult.getResult() == ErrorCode.BUSINESS_EXCEPTION) {
            System.out.println(JSONObject.toJSONString(dubboResult));
            return;
        }
        for (CommandExecuteDto commandExecuteDto : dubboResult.getRe()) {
            System.out.println(JSONObject.toJSONString(commandExecuteDto));
        }
    }

}
