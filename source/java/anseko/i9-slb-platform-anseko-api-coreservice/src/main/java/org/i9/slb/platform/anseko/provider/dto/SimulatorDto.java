package org.i9.slb.platform.anseko.provider.dto;

import org.i9.slb.platform.anseko.common.types.PowerStateEnum;

/**
 * 模拟器dto
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class SimulatorDto implements java.io.Serializable {

    public PowerStateEnum getPowerState() {
        return PowerStateEnum.valueOf(this.powerStatus);
    }

    private static final long serialVersionUID = -3963469857629881946L;

    private String id;

    private String simulatorName;

    private String createDate;

    private String updateDate;

    private Integer cpunum;

    private Integer ramnum;

    /**
     * 电源状态
     *
     * @see org.i9.slb.platform.anseko.common.types.PowerStateEnum
     */
    private Integer powerStatus;

     private Integer androidVersion;

    private String instanceId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    public Integer getCpunum() {
        return cpunum;
    }

    public void setCpunum(Integer cpunum) {
        this.cpunum = cpunum;
    }

    public Integer getRamnum() {
        return ramnum;
    }

    public void setRamnum(Integer ramnum) {
        this.ramnum = ramnum;
    }

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    public Integer getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(Integer androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
