package org.i9.slb.platform.anseko.provider.remote;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.downstream.dto.result.CommandExecuteReDto;
import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.i9.slb.platform.anseko.provider.dto.CommandCallbackDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 调用coreservice远程服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:16
 */
@Service
public class CoreserviceRemoteService {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    public void remoteServiceCallbackCommandDispatch(CommandExecuteReDto commandExecuteReDto) {
        try {
            CommandCallbackDto commandCallbackDto = new CommandCallbackDto();
            commandCallbackDto.setCommandId(commandExecuteReDto.getCommandId());
            commandCallbackDto.setExecuteResult(commandExecuteReDto.getExecuteResult());
            dubboCommandRemoteService.callbackCommandExecute(commandCallbackDto);
        } catch (Exception e) {
            LOGGER.info("执行命令回调失败, param : {}", JSONObject.toJSONString(commandExecuteReDto));
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(CoreserviceRemoteService.class);
}
