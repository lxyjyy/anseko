package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.downstream.IMultipleCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.provider.task.MultipleCommandExecuteTask;
import org.i9.slb.platform.anseko.provider.task.pool.CommandExecuteSchedulerPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 命令执行远程调用服务
 *
 * @author R12
 * @date 2018.08.28
 */
@Service("multipleCommandExecuteRemoteService")
public class MultipleCommandExecuteRemoteService implements IMultipleCommandExecuteRemoteService {

    @Autowired
    private CommandExecuteSchedulerPool pool;

    /**
     * 多命令执行调用
     *
     * @param clientId
     * @param groupCommandParamDto
     */
    @Override
    public void multipleCommandExecute(String clientId, GroupCommandParamDto groupCommandParamDto) {
        MultipleCommandExecuteTask multipleCommandExecuteTask = new MultipleCommandExecuteTask(groupCommandParamDto);
        pool.submit(multipleCommandExecuteTask);
    }
}
