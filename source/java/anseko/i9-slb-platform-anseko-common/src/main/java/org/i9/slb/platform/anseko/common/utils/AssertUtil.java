package org.i9.slb.platform.anseko.common.utils;

import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.springframework.util.Assert;

/**
 * 断言工具类
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/21 10:27
 */
public class AssertUtil {

    public static void isTrue(boolean expression, String message) throws BusinessException {
        try {
            Assert.isTrue(expression);
        } catch (IllegalArgumentException e) {
            throw new BusinessException(message);
        }
    }
}
