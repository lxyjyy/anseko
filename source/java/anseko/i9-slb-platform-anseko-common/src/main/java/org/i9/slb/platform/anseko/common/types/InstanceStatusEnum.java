package org.i9.slb.platform.anseko.common.types;

/**
 * 实例状态枚举类
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/21 17:19
 */
public enum InstanceStatusEnum {

    ENABLE(1, "启用"), DISABLE(2, "禁用");

    InstanceStatusEnum(int index, String name) {
        this.index = index;
        this.name = name;
    }

    private final String name;

    private final int index;

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public static InstanceStatusEnum valueOf(Integer status) {
        for (InstanceStatusEnum instanceStatusEnum : values()) {
            if (status == instanceStatusEnum.getIndex()) {
                return instanceStatusEnum;
            }
        }
        return InstanceStatusEnum.ENABLE;
    }
}
