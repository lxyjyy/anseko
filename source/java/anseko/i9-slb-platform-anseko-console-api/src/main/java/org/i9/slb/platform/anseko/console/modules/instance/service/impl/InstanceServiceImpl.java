package org.i9.slb.platform.anseko.console.modules.instance.service.impl;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.console.modules.instance.bean.*;
import org.i9.slb.platform.anseko.console.modules.instance.service.InstanceService;
import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceListDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceSearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 实例管理服务类
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/21 16:11
 */
@Service("instanceServiceImpl")
public class InstanceServiceImpl implements InstanceService {

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    /**
     * 获取实例列表
     *
     * @return
     */
    @Override
    public List<InstanceInfoView> getInstanceList() {
        DubboResult<List<InstanceDto>> dubboResult = this.dubboInstanceRemoteService.getInstanceDtos();
        dubboResult.tryBusinessException();

        List<InstanceInfoView> list = new ArrayList<InstanceInfoView>();
        for (InstanceDto instanceDto : dubboResult.getRe()) {
            InstanceInfoView instanceInfoView = new InstanceInfoView();
            instanceInfoView.copyProperty(instanceDto);
            list.add(instanceInfoView);
        }
        return list;
    }

    /**
     * 获取实例信息
     *
     * @param instanceId
     * @return
     */
    @Override
    public InstanceInfoView getInstanceInfo(String instanceId) {
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(instanceId);
        dubboResult.tryBusinessException();

        InstanceInfoView instanceInfoView = new InstanceInfoView();
        instanceInfoView.copyProperty(dubboResult.getRe());
        return instanceInfoView;
    }

    /**
     * 创建实例信息
     *
     * @param instanceSaveForm
     */
    @Override
    public void createInstanceInfo(InstanceSaveForm instanceSaveForm) {
        InstanceDto instanceDto = new InstanceDto();
        instanceDto.setId(UUIDUtil.generateUUID());
        instanceDto.setInstanceName(instanceSaveForm.getInstanceName());
        instanceDto.setRemoteAddress(instanceSaveForm.getRemoteAddress());
        instanceDto.setVirtualType(instanceSaveForm.getVirtualType());

        DubboResult<?> dubboResult = this.dubboInstanceRemoteService.createInstanceInfo(instanceDto);
        dubboResult.tryBusinessException();
    }

    /**
     * 更新实例信息
     *
     * @param instanceUpdateForm
     */
    @Override
    public void updateInstanceInfo(InstanceUpdateForm instanceUpdateForm) {
        InstanceDto instanceDto = new InstanceDto();
        instanceDto.setId(instanceUpdateForm.getId());
        instanceDto.setInstanceName(instanceUpdateForm.getInstanceName());
        instanceDto.setRemoteAddress(instanceUpdateForm.getRemoteAddress());
        instanceDto.setVirtualType(instanceUpdateForm.getVirtualType());

        DubboResult<?> dubboResult = this.dubboInstanceRemoteService.updateInstanceInfo(instanceDto);
        dubboResult.tryBusinessException();
    }

    @Override
    public InstanceListView getInstanceListPage(InstanceSearch instanceSearch) {
        InstanceSearchDto instanceSearchDto = new InstanceSearchDto();
        instanceSearchDto.setStartPage(instanceSearch.getStartPage());
        instanceSearchDto.setPageSize(instanceSearch.getPageSize());
        instanceSearchDto.setInstanceName(instanceSearch.getInstanceName());

        DubboResult<InstanceListDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDtosPage(instanceSearchDto);
        dubboResult.tryBusinessException();

        List<InstanceInfoView> list = new ArrayList<InstanceInfoView>();
        InstanceListDto instanceListDto = dubboResult.getRe();
        for (InstanceDto instanceDto : instanceListDto.getList()) {
            InstanceInfoView instanceInfoView = new InstanceInfoView();
            instanceInfoView.copyProperty(instanceDto);
            list.add(instanceInfoView);
        }

        InstanceListView instanceListView = new InstanceListView();
        instanceListView.setList(list);
        instanceListView.setTotalRow(instanceListDto.getTotalRow());
        return instanceListView;
    }

    @Override
    public void updateInstanceStatus(String instanceId, Integer status) {
        DubboResult<?> dubboResult = this.dubboInstanceRemoteService.updateInstanceStatus(instanceId, status);
        dubboResult.tryBusinessException();
    }

    @Override
    public List<InstanceInfoView> getIstanceListOnline() {
        DubboResult<List<InstanceDto>> dubboResult = this.dubboInstanceRemoteService.getIstanceDtosOnline();
        dubboResult.tryBusinessException();

        List<InstanceInfoView> list = new ArrayList<>();
        for (InstanceDto instanceDto : dubboResult.getRe()) {
            InstanceInfoView instanceInfoView = new InstanceInfoView();
            instanceInfoView.copyProperty(instanceDto);
            list.add(instanceInfoView);
        }
        return list;
    }
}
