package org.i9.slb.platform.anseko.console.utils;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.common.types.VirtualEnum;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorBuilderHandle;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorHardwareHandle;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorNetworkPortHandle;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorOperateHandle;
import org.i9.slb.platform.anseko.hypervisors.kvm.KvmSimulatorBuilderHandle;
import org.i9.slb.platform.anseko.hypervisors.kvm.KvmSimulatorHardwareHandle;
import org.i9.slb.platform.anseko.hypervisors.kvm.KvmSimulatorNetworkPortHandle;
import org.i9.slb.platform.anseko.hypervisors.kvm.KvmSimulatorOperateHandle;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;
import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 构建器工厂
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/6 16:28
 */
@Service
public class SimulatorEnvironmentFactory {

    @Autowired
    private DownStreamRemoteCommand downStreamRemoteCommand;

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    public SimulatorNetworkPortHandle makeSimulatorNetworkPortHandle(SimulatorDto simulatorDto) {
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(simulatorDto.getInstanceId());
        dubboResult.tryBusinessException();
        InstanceDto instanceDto = dubboResult.getRe();
        if (instanceDto.getVirtualEnum() == VirtualEnum.KVM) {
            return new KvmSimulatorNetworkPortHandle(simulatorDto.getId(), downStreamRemoteCommand);
        }
        throw new BusinessException("模拟构建器生成失败");
    }

    /**
     * 生成模拟器构建器
     *
     * @param simulatorInfo
     * @return
     */
    public SimulatorBuilderHandle makeSimulatorBuilderHandle(SimulatorInfo simulatorInfo) {
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(simulatorInfo.getInstanceId());
        dubboResult.tryBusinessException();
        InstanceDto instanceDto = dubboResult.getRe();
        if (instanceDto.getVirtualEnum() == VirtualEnum.KVM) {
            return new KvmSimulatorBuilderHandle(simulatorInfo, downStreamRemoteCommand);
        }
        throw new BusinessException("模拟构建器生成失败");
    }

    /**
     * 生成模拟器操作器
     *
     * @param simulatorDto
     * @return
     */
    public SimulatorOperateHandle makeSimulatorOperateHandle(SimulatorDto simulatorDto) {
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(simulatorDto.getInstanceId());
        dubboResult.tryBusinessException();
        InstanceDto instanceDto = dubboResult.getRe();
        if (instanceDto.getVirtualEnum() == VirtualEnum.KVM) {
            return new KvmSimulatorOperateHandle(simulatorDto.getId(), downStreamRemoteCommand);
        }
        throw new BusinessException("模拟构建器生成失败");
    }

    /**
     * 生成模拟器操作器
     *
     * @param simulatorName
     * @param instanceId
     * @return
     */
    public SimulatorHardwareHandle makeSimulatorHardwareHandle(String simulatorName, String instanceId) {
        DubboResult<InstanceDto> dubboResult = this.dubboInstanceRemoteService.getInstanceDto(instanceId);
        dubboResult.tryBusinessException();
        InstanceDto instanceDto = dubboResult.getRe();
        if (instanceDto.getVirtualEnum() == VirtualEnum.KVM) {
            return new KvmSimulatorHardwareHandle(simulatorName);
        }
        throw new BusinessException("模拟构建器生成失败");
    }
}
