package org.i9.slb.platform.anseko.console.modules.instance.bean;

/**
 * @author R12
 * @version 1.0
 * @date 2019/2/21 16:12
 */
public class InstanceUpdateForm implements java.io.Serializable {

    private static final long serialVersionUID = 6705065387907848191L;

    private String id;
    /**
     * 实例名称
     */
    private String instanceName;
    /**
     * 虚拟化类型
     *
     * @see org.i9.slb.platform.anseko.common.types.VirtualEnum
     */
    private Integer virtualType;
    /**
     * 远程地址
     */
    private String remoteAddress;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public Integer getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(Integer virtualType) {
        this.virtualType = virtualType;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
}
