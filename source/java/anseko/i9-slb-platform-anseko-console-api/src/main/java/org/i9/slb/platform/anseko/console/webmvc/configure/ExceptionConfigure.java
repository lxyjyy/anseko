package org.i9.slb.platform.anseko.console.webmvc.configure;

import org.i9.slb.platform.anseko.common.constant.ErrorCode;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 异常配置
 *
 * @author r12
 * @date 2019年2月12日 11:12:54
 */
@ControllerAdvice
@ResponseBody
public class ExceptionConfigure {

    @ExceptionHandler(BusinessException.class)
    public Object exceptionHandler(BusinessException e, HttpServletResponse response) throws IOException {
        HttpResult<?> httpResult = new HttpResult();
        httpResult.setMessage(e.getMessage());
        httpResult.setResult(ErrorCode.BUSINESS_EXCEPTION);
        return httpResult;
    }
}
