package org.i9.slb.platform.anseko.console.utils;

public class BaseSearch implements java.io.Serializable {

    private static final long serialVersionUID = -4713014913581879180L;

    /**
     * 起始page
     */
    private int startPage;
    /**
     * 当页总数
     */
    private int pageSize;

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
