package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.common.constant.CommandExecuteStatusEnum;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;

/**
 * 命令执行器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:52
 */
public class CommandExecuteInfoView implements java.io.Serializable {

    public String getStatusDesc() {
        CommandExecuteStatusEnum commandExecuteStatusEnum = CommandExecuteStatusEnum.valueOf(this.status);
        return commandExecuteStatusEnum.getName();
    }

    private static final long serialVersionUID = 1529869248423372866L;
    /**
     * 执行位置
     */
    private Integer posIndex;
    /**
     * 执行组编号
     */
    private String commandGroupId;
    /**
     * 命令执行编号
     */
    private String commandId;
    /**
     * 命令行
     */
    private String commandLine;
    /**
     * 执行结果
     */
    private String commandResult;
    /**
     * 开始日期
     */
    private String startDate;
    /**
     * 结束日期
     */
    private String endDate;
    /**
     * 状态
     *
     * @see org.i9.slb.platform.anseko.common.constant.CommandExecuteStatusEnum
     */
    private Integer status;

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(String commandResult) {
        this.commandResult = commandResult;
    }

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void copyProperty(CommandExecuteDto commandExecuteDto) {
        this.commandGroupId = commandExecuteDto.getCommandGroupId();
        this.commandId = commandExecuteDto.getCommandId();
        this.commandLine = commandExecuteDto.getCommandLine();
        this.commandResult = commandExecuteDto.getCommandResult();
        this.startDate = commandExecuteDto.getStartDate();
        this.endDate = commandExecuteDto.getEndDate();
        this.status = commandExecuteDto.getStatus();
        this.posIndex = commandExecuteDto.getPosIndex();
    }
}
