package org.i9.slb.platform.anseko.console.modules.user.bean;

import org.i9.slb.platform.anseko.console.utils.BaseSearch;

public class UserSearch extends BaseSearch implements java.io.Serializable {

    private static final long serialVersionUID = 2235009355959520976L;

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
