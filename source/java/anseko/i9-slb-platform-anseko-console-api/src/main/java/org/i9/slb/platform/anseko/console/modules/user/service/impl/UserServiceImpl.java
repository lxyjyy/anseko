package org.i9.slb.platform.anseko.console.modules.user.service.impl;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.common.utils.Md5Util;
import org.i9.slb.platform.anseko.console.modules.common.bean.UserLoginView;
import org.i9.slb.platform.anseko.console.modules.user.bean.*;
import org.i9.slb.platform.anseko.console.modules.user.service.UserService;
import org.i9.slb.platform.anseko.console.modules.user.service.UserTokenRedisService;
import org.i9.slb.platform.anseko.provider.IDubboUserRemoteService;
import org.i9.slb.platform.anseko.provider.dto.UserDto;
import org.i9.slb.platform.anseko.provider.dto.UserListDto;
import org.i9.slb.platform.anseko.provider.dto.UserPasswordDto;
import org.i9.slb.platform.anseko.provider.dto.UserSearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 用户操作service
 *
 * @author r12
 * @date 2019年2月21日 11:25:03
 */
@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserTokenRedisService userTokenRedisService;

    @Autowired
    private IDubboUserRemoteService dubboUserRemoteService;

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    @Override
    public UserLoginView userLogin(String username, String password) {
        DubboResult<UserDto> dubboResult = this.dubboUserRemoteService.getUserDtoByUsername(username);
        dubboResult.tryBusinessException();

        UserDto userDto = dubboResult.getRe();
        String md5Str = Md5Util.MD5(password);
        if (!userDto.getPassword().equals(md5Str)) {
            throw new BusinessException("用户密码不正确");
        }

        UserInfoView userInfoView = new UserInfoView();
        userInfoView.setId(userDto.getUsername());
        userInfoView.setUsername(userDto.getUsername());
        userInfoView.setNickname(userDto.getNickname());
        userInfoView.setPortraitUrl(userDto.getPortraitUrl());

        // 保存用户会话数据
        String userToken = this.userTokenRedisService.createUserTokenToRedisDb(userDto);

        UserLoginView userLoginView = new UserLoginView();
        userLoginView.setUserInfoView(userInfoView);
        userLoginView.setUserToken(userToken);

        return userLoginView;
    }

    /**
     * 获取用户列表
     *
     * @param userSearch
     * @return
     */
    @Override
    public UserListView getUserInfoList(UserSearch userSearch) {
        UserSearchDto userSearchDto = new UserSearchDto();
        userSearchDto.setStartPage(userSearch.getStartPage());
        userSearchDto.setPageSize(userSearch.getPageSize());
        userSearchDto.setUsername(userSearch.getUsername());

        DubboResult<UserListDto> dubboResult = this.dubboUserRemoteService.getUserDtos(userSearchDto);
        dubboResult.tryBusinessException();

        UserListDto userListDto = dubboResult.getRe();
        List<UserInfoView> list = new ArrayList<UserInfoView>();
        for (UserDto userDto : userListDto.getList()) {
            UserInfoView userInfoView = new UserInfoView();
            userInfoView.copyProperty(userDto);
            list.add(userInfoView);
        }

        UserListView userListView = new UserListView();
        userListView.setList(list);
        userListView.setTotalRow(userListDto.getTotalRow());

        return userListView;
    }

    /**
     * 获取用户信息
     *
     * @param id
     * @return
     */
    @Override
    public UserInfoView getUserInfo(String id) {
        DubboResult<UserDto> dubboResult = this.dubboUserRemoteService.getUserDtoById(id);
        dubboResult.tryBusinessException();

        UserDto userDto = dubboResult.getRe();
        UserInfoView userInfoView = new UserInfoView();
        userInfoView.copyProperty(userDto);

        return userInfoView;
    }

    /**
     * 创建用户信息
     *
     * @param userSaveForm
     */
    @Override
    public void createUserInfo(UserSaveForm userSaveForm) {
        DubboResult<?> dubboResult = this.dubboUserRemoteService.checkUsernameAlreadyExist(userSaveForm.getUsername());
        dubboResult.tryBusinessException();

        UserDto userDto = new UserDto();
        userDto.setId(UUID.randomUUID().toString());
        userDto.setUsername(userSaveForm.getUsername());
        userDto.setPassword(userSaveForm.getPassword());
        userDto.setNickname(userSaveForm.getNickname());
        userDto.setPortraitUrl(userSaveForm.getPortraitUrl());

        dubboResult = this.dubboUserRemoteService.createUserInfo(userDto);
        dubboResult.tryBusinessException();
    }

    /**
     * 更新用户信息
     *
     * @param userUpdateForm
     */
    @Override
    public void updateUser(UserUpdateForm userUpdateForm) {
        UserDto userDto = new UserDto();
        userDto.setNickname(userUpdateForm.getNickname());
        userDto.setPortraitUrl(userUpdateForm.getPortraitUrl());
        userDto.setId(userUpdateForm.getId());
        DubboResult<?> dubboResult = this.dubboUserRemoteService.updateUserInfo(userDto);
        dubboResult.tryBusinessException();
    }

    /**
     * 删除用户
     *
     * @param userId
     */
    @Override
    public void deleteUser(String userId) {
        DubboResult<?> dubboResult = this.dubboUserRemoteService.deleteUserById(userId);
        dubboResult.tryBusinessException();
    }

    /**
     * 更新用户密码
     *
     * @param userId
     * @param password
     */
    @Override
    public void changeUserPassword(String userId, String password) {
        UserPasswordDto userPasswordDto = new UserPasswordDto();
        userPasswordDto.setUserId(userId);
        userPasswordDto.setPassword(password);
        DubboResult<?> dubboResult = this.dubboUserRemoteService.changeUserPassword(userPasswordDto);
        dubboResult.tryBusinessException();
    }

    @Override
    public void userLogout(String userToken) {
        this.userTokenRedisService.deleteUserToken(userToken);
    }
}
